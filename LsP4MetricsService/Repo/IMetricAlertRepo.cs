﻿using System.Collections.Generic;
using LsP4MetricsService.Model;
using MetricsDash.Models;

namespace LsP4MetricsService.Repo
{
    public interface IMetricAlertRepo
    {
        IList<Alert> GetMetricAlerts(RealtimeStatConfig metric);
    }
}