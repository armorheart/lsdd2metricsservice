﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using LsP4MetricsService.Config;
using LsP4MetricsService.Email;
using LsP4MetricsService.Model;
using MetricsDash.Models;
using MetricsDash.Repo;
using MetricsDash.Repo.Interfaces;
using MetricsDash.UserManagement;
using Phase4Lib.Extensions;
using Phase4Lib.Web.DataAccess;
using Phase4Lib.Web.Models;

namespace LsP4MetricsService.Repo
{
    /// <summary>
    /// Class to Get Statistics
    /// </summary>
    public class MetricsServiceRepo : IMetricsServiceRepo
    {
        public string MasterConStr { get; set; }
        public Dictionary<string, ConnectorConfig> RegionConnStrs { get; private set; }

        public List<ErrorVm>  Errors { get; private set; }


//        private const string InsertResultSql =
//            @"INSERT INTO UDO_DD2_METRICS_HISTORY ( TIMESTAMP, SERVER, SERIES_ID )
//            VALUES( @timestamp, @server, @seriesId) ";

        private const string InsertResultIntervalSql =
           @"INSERT INTO UDO_DD2_METRICS_HISTORY ( TIMESTAMP, SERVER, SERIES_ID, VARIABLE_BEGIN_INTERVAL, VARIABLE_END_INTERVAL, VARIABLE_LNLDBID )
            VALUES( @timestamp, @server, @seriesId, @beginInterval, @endInterval, @databaseId) ";

        private const string UpdateResultsSql =
            @"UPDATE UDO_DD2_METRICS_HISTORY SET {0} = @result 
            WHERE SERIES_ID = @seriesId AND SERVER = @server ";

        private const string EarlyStatSql = @"select min(TIMESTAMP) as early
            from UDO_DD2_METRICS_HISTORY
            where SERVER = @server
            and {0} is not null";

        private readonly IHistoryRepo _Repo;
        private readonly IUserRepo _UserRepo;
        private readonly IMetricsRepo<StatConfig> _MetricsRepo;
        private readonly IMetricsRepo<RealtimeStatConfig> _RealtimeRepo; 

        private readonly ICommonRepo _CommonRepo;
        //private Phase4Lib.Logging.EventLogLogger _Log;

        private static readonly log4net.ILog _Log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public MetricsServiceRepo(string connStr, 
            ICommonRepo commonRepo,
            IHistoryRepo historyRepo,
            IMetricsRepo<StatConfig> metricsRepo,
            IMetricsRepo<RealtimeStatConfig> realtimeMetricsRepo,
            IUserRepo userRepo)
        {
            _CommonRepo = commonRepo;
            RegionConnStrs = new Dictionary<string, ConnectorConfig>();
            MasterConStr = connStr;
            _Repo = historyRepo;
            _UserRepo = userRepo;
            _MetricsRepo = metricsRepo;
            _RealtimeRepo = realtimeMetricsRepo;
        }

        /// <summary>
        /// Gets list of statistics to gather
        /// Each stat has it's own query
        /// </summary>
        /// <returns>A List of enabled metrics</returns>
        public IEnumerable<StatConfig> GetMetricList()
        {
            return _MetricsRepo.GetMetrics();
        }

        public IEnumerable<RealtimeStatConfig> GetRealtimeList()
        {
            return _RealtimeRepo.GetMetrics();
        }

        public void InitRegions()
        {
            var regions = _CommonRepo.GetRegions();
            foreach (var region in regions)
            {
                if (!RegionConnStrs.ContainsKey(region.Id.ToString()))
                {
                    RegionConnStrs.Add(region.Id.ToString(), region);    
                }
                
            }
        }

        public int InitHistory()
        {
            return _Repo.InitHistoryTable();
        }

        public void InitEarlyDate()
        {
            foreach (var metric in GetMetricList().Where(m => m.EarliestStatDate == null))
            {
                try
                {
                    DateTime? eDate = GetEarlyStatDate(metric);
                    metric.EarliestStatDate = eDate ?? DateTime.UtcNow;
                    _MetricsRepo.UpdateMetric(metric, false);
                    _Log.Info(string.Format("Set Early Stat Date for {0} to {1}", metric.ColumnName, metric.EarliestStatDate));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
      

        /// <summary>
        /// Inserts lines into the History table for the current series
        /// </summary>
        /// <param name="seriesTimestamp"></param>
        /// <param name="seriesId"></param>
        public void InsertHistorySeries(DateTime seriesTimestamp, Guid seriesId, Interval interval)
        {
            //Get all Regions
            var regions = _CommonRepo.GetRegions();
            foreach (var r in regions)
            {
                InsertHistoryRegion(InsertResultIntervalSql, r.Name, seriesId, seriesTimestamp, interval);
            }

            //Initialize the Errors list
            Errors = new List<ErrorVm>();
           
        }

        /// <summary>
        /// Runs a metric and updates history table
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="seriesTimestamp"></param>
        /// <param name="id"></param>
        /// <param name="seriesId"></param>
        /// <returns></returns>
        public bool RunMetric(StatConfig stat, DateTime seriesTimestamp, Guid seriesId)
        {
            if (!string.IsNullOrEmpty(stat.ServerSel))
            {
                foreach (var server in stat.ServerSel.Split(','))
                {
                    
                    RunQuery(stat, server, seriesId, seriesTimestamp);
                }
                

                //Check / Add Early Stat Date
                if (stat.EarliestStatDate == null)
                {
                    DateTime? eDate = GetEarlyStatDate(stat);
                    stat.EarliestStatDate = eDate ?? seriesTimestamp;
                    UpdateStat(stat);
                    _Log.Info(string.Format("Set Early Stat Date for {0} to {1}", stat.ColumnName, stat.EarliestStatDate));
                }
            }

            return true;
        }

        public bool RunRealTime(RealtimeStatConfig stat)
        {
            if (!string.IsNullOrEmpty(stat.ServerSel))
            {
                var seriesTimeStamp = DateTime.UtcNow;
                foreach (var server in stat.ServerSel.Split(','))
                {
                    try
                    {
                        _RealtimeRepo.RunMetricQuery(stat, server, seriesTimeStamp);
                    }
                    catch (Exception ex)
                    {
                        _Log.Error(string.Format("Running Query {0} on {1}", stat.ColumnName, server), ex);

                        if (Errors == null)
                        {
                            Errors = new List<ErrorVm>();
                        }

                        Errors.Add(new ErrorVm()
                        {
                            Error = ex.Message,
                            Exception = ex,
                            Stat = stat.ColumnName,
                            TimeStamp =seriesTimeStamp,
                            Server = RegionConnStrs[server].Name
                        });
                    }
                    
                }
            }

            return true;
        }

        /// <summary>
        /// Runs the Metric Query
        /// </summary>
        /// <param name="stat">Metric</param>
        /// <param name="region">Region name or Master</param>
        /// <param name="seriesId"></param>
        /// <param name="timestamp"></param>
        /// <param name="databaseId"></param>
        private void RunQuery(StatConfig stat, string region, Guid seriesId, DateTime timestamp)
        {
            //var connStr = (region == "Master") ? MasterConStr : RegionConnStrs[region];
            var connector = RegionConnStrs[region];
            var databaseId = GetDatabaseId(connector.Name);


            using (var conn = new SqlConnection(connector.GetConnectionString()))
            {
                using (var cmd = new SqlCommand(stat.Query, conn))
                {
                    try
                    {
                        conn.Open();

                        //Add interval parameters
                        //They'll be ignored if not needed
                        cmd.Parameters.AddWithValue("@BEGIN_INTERVAL", GetBeginInterval(timestamp, (Interval)stat.Interval));
                        cmd.Parameters.AddWithValue("@END_INTERVAL", GetEndInterval(timestamp, (Interval)stat.Interval));
                        cmd.Parameters.AddWithValue("@LNL_DBID", databaseId);

                        var result = (int)cmd.ExecuteScalar();
                        //write result to stat.ColumnName setting Region Column
                        AddResult(stat, connector.Name, result, seriesId, timestamp);

                        //Log to the History Log
                        _Log.Info(string.Format("SUCCESS: {0} : {1} : {2} ", stat.ColumnName, region, result));
                    }
                    catch (Exception ex)
                    {
                        _Log.Error(string.Format("Running Query {0} on {1}", stat.ColumnName, region), ex);

                        if (Errors == null)
                        {
                            Errors = new List<ErrorVm>();
                        }

                        Errors.Add(new ErrorVm()
                        {
                            Error = ex.Message,
                            Exception = ex,
                            Stat = stat.ColumnName,
                            TimeStamp = DateTime.Now,
                            Server = region
                        });
                    }
                    

                }
            }
        }

        public DateTime GetBeginInterval(DateTime timestamp, Interval interval)
        {
            switch (interval)
            {
                case Interval.Daily:
                    return timestamp.AddDays(-1);
                case Interval.Weekly:
                    return timestamp.StartofWeek(DayOfWeek.Sunday);
                case Interval.Monthly:
                    return timestamp.StartOfMonth().AddMonths(-1);
                default:
                    throw new ArgumentOutOfRangeException("interval");
            }
        }

        public DateTime GetEndInterval(DateTime timestamp, Interval interval)
        {
            switch (interval)
            {
                case Interval.Daily:
                    return timestamp.AddHours(-1);
                case Interval.Weekly:
                    return timestamp.EndOfWeek(DayOfWeek.Sunday);
                case Interval.Monthly:
                    return timestamp.StartOfMonth().AddDays(-1);
                default:
                    throw new ArgumentOutOfRangeException("interval");
            }
        }

        /// <summary>
        /// This may be incorrect if the Metrics Database is different then the OnGuard Database
        /// </summary>
        /// <param name="region"></param>
        /// <returns></returns>
        private int GetDatabaseId(string region)
        {
            //Get databaseId from Lnl_Db
            using (var context = new MetricsDashDataContext(MasterConStr))
            {
                var dbId = context.LNL_DBs.SingleOrDefault(lnl => lnl.NODE_NAME.ToLower() == region.ToLower());
                return dbId == null ? 1 : dbId.LNL_DBID;
            }
        }

        private void InsertHistoryRegion(string sql, string region, Guid seriesId, DateTime timestamp, Interval interval)
        {
            
            using (var conn = new SqlConnection(MasterConStr))
            {
                using (var cmd = new SqlCommand(sql, conn))
                {
                    conn.Open();

                    cmd.Parameters.AddWithValue("@timestamp", timestamp);
                    cmd.Parameters.AddWithValue("@server", region.ToUpper());
                    cmd.Parameters.AddWithValue("@seriesId", seriesId);
                    cmd.Parameters.AddWithValue("@beginInterval", GetBeginInterval(timestamp, interval));
                    cmd.Parameters.AddWithValue("@endInterval", GetEndInterval(timestamp, interval));
                    cmd.Parameters.AddWithValue("@databaseId", GetDatabaseId(region));


                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        //Log 

                    }

                }
            }
        }

        /// <summary>
        /// Writes the Query result to the History Table
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="region">Master or region name</param>
        /// <param name="result"></param>
        /// <param name="seriesId"></param>
        /// <param name="timestamp"></param>
        private void AddResult(StatConfig stat, string region, int result, Guid seriesId, DateTime timestamp)
        {
            var sql  = string.Format(UpdateResultsSql, stat.ColumnName);    
            
            using (var conn = new SqlConnection(MasterConStr))
            {
                using (var cmd = new SqlCommand(sql, conn))
                {
                    conn.Open();

                    cmd.Parameters.AddWithValue("@server", region.ToUpper());
                    cmd.Parameters.AddWithValue("@seriesId", seriesId);
                    cmd.Parameters.AddWithValue("@result", result);

                    cmd.ExecuteNonQuery();
                    //Update the last run date
                    stat.LastRunDate = timestamp;
                    UpdateStat(stat);
                }
            }
        }

        private void UpdateStat(StatConfig stat)
        {
            _MetricsRepo.UpdateMetric(stat, false);
        }

        private void UpdateStat(RealtimeStatConfig stat)
        {
            _RealtimeRepo.UpdateMetric(stat, false);
        }

        private DateTime? GetEarlyStatDate(StatConfig metric)
        {
            using (var conn = new SqlConnection(MasterConStr))
            {
                var sql = string.Format(EarlyStatSql, metric.ColumnName);
                using (var cmd = new SqlCommand(sql, conn))
                {
                    var servers = metric.ServerSel.Split(',');
                    var svrParam = RegionConnStrs[servers.First()];
                    cmd.Parameters.AddWithValue("@server", svrParam.Name);

                    conn.Open();
                    return cmd.ExecuteScalar() as DateTime?;
                }
            }
        }

        internal void CheckAlerts(EmailConfig emailConfig)
        {
            var users = _UserRepo.GetUsers();

            //get last 2 record(s) from history
            var ts = _Repo.GetLastHistoryDate();
            var dataPoints = _Repo.GetHistory(null, null, ts.Last(), null);

            //total the datapoints into a single list
            var combined = new List<MetricShort>();
            foreach (var metrics in dataPoints)
            {
                combined.AddRange(metrics.Metrics);
            }

            var emailService = new EmailService();

            //for each user, get the metrics they're subscribed to
            foreach (var user in users)
            {
                var subscribed = from m in combined
                                 join a in user.AlertPrefs.Preferences
                                     on m.ColumnName equals a.SubscriptionId
                                 join fullMetric in _MetricsRepo.GetMetrics()
                                     on m.ColumnName equals fullMetric.ColumnName
                                 select new MetricVM(m.Value, fullMetric);

                //Send metric summary report
                emailService.SendSummaryEmail(new Emailsettings
                    {
                        From = emailConfig.From,
                        Server = emailConfig.Server,
                        Subject = "NEED NEW SUBJECT",
                        To = user.Email
                    }, subscribed );

            }
        }
    }
}
