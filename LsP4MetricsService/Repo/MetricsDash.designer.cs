﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18063
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LsP4MetricsService.Repo
{
    using System.Data.Linq.Mapping;
    using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="AccessControl_RCDN_SEC")]
	public partial class MetricsDashDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertMetricConfig(MetricConfig instance);
    partial void UpdateMetricConfig(MetricConfig instance);
    partial void DeleteMetricConfig(MetricConfig instance);
    partial void InsertLNL_DB(LNL_DB instance);
    partial void UpdateLNL_DB(LNL_DB instance);
    partial void DeleteLNL_DB(LNL_DB instance);
    #endregion
		
		public MetricsDashDataContext() : 
				base(global::LsP4MetricsService.Properties.Settings.Default.AccessControl_RCDN_SECConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public MetricsDashDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public MetricsDashDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public MetricsDashDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public MetricsDashDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<MetricConfig> MetricConfigs
		{
			get
			{
				return this.GetTable<MetricConfig>();
			}
		}
		
		public System.Data.Linq.Table<LNL_DB> LNL_DBs
		{
			get
			{
				return this.GetTable<LNL_DB>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.UDO_DD2_METRICS_STAT_CONFIG")]
	public partial class MetricConfig : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ID;
		
		private string _COLUMN_NAME;
		
		private string _DISPLAY_NAME;
		
		private string _DESCRIPTION;
		
		private int _CATEGORY1;
		
		private int _CATEGORY2;
		
		private string _QUERY;
		
		private int _SERVER_SELECTION;
		
		private bool _INDV_STAT;
		
		private System.Nullable<int> _MIN_VALUE;
		
		private System.Nullable<int> _MAX_VALUE;
		
		private System.Nullable<int> _INC_TOLERANCE;
		
		private System.Nullable<int> _DEC_TOLERANCE;
		
		private int _INTERVAL;
		
		private int _STATUS;
		
		private System.DateTime _CREATE_DATE;
		
		private System.Nullable<System.DateTime> _LAST_RUN_DATE;
		
		private bool _SYSTEM_METRIC;
		
		private System.Nullable<System.DateTime> _EARLY_STAT_DATE;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnCOLUMN_NAMEChanging(string value);
    partial void OnCOLUMN_NAMEChanged();
    partial void OnDISPLAY_NAMEChanging(string value);
    partial void OnDISPLAY_NAMEChanged();
    partial void OnDESCRIPTIONChanging(string value);
    partial void OnDESCRIPTIONChanged();
    partial void OnCATEGORY1Changing(int value);
    partial void OnCATEGORY1Changed();
    partial void OnCATEGORY2Changing(int value);
    partial void OnCATEGORY2Changed();
    partial void OnQUERYChanging(string value);
    partial void OnQUERYChanged();
    partial void OnSERVER_SELECTIONChanging(int value);
    partial void OnSERVER_SELECTIONChanged();
    partial void OnINDV_STATChanging(bool value);
    partial void OnINDV_STATChanged();
    partial void OnMIN_VALUEChanging(System.Nullable<int> value);
    partial void OnMIN_VALUEChanged();
    partial void OnMAX_VALUEChanging(System.Nullable<int> value);
    partial void OnMAX_VALUEChanged();
    partial void OnINC_TOLERANCEChanging(System.Nullable<int> value);
    partial void OnINC_TOLERANCEChanged();
    partial void OnDEC_TOLERANCEChanging(System.Nullable<int> value);
    partial void OnDEC_TOLERANCEChanged();
    partial void OnINTERVALChanging(int value);
    partial void OnINTERVALChanged();
    partial void OnSTATUSChanging(int value);
    partial void OnSTATUSChanged();
    partial void OnCREATE_DATEChanging(System.DateTime value);
    partial void OnCREATE_DATEChanged();
    partial void OnLAST_RUN_DATEChanging(System.Nullable<System.DateTime> value);
    partial void OnLAST_RUN_DATEChanged();
    partial void OnSYSTEM_METRICChanging(bool value);
    partial void OnSYSTEM_METRICChanged();
    partial void OnEARLY_STAT_DATEChanging(System.Nullable<System.DateTime> value);
    partial void OnEARLY_STAT_DATEChanged();
    #endregion
		
		public MetricConfig()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_COLUMN_NAME", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string COLUMN_NAME
		{
			get
			{
				return this._COLUMN_NAME;
			}
			set
			{
				if ((this._COLUMN_NAME != value))
				{
					this.OnCOLUMN_NAMEChanging(value);
					this.SendPropertyChanging();
					this._COLUMN_NAME = value;
					this.SendPropertyChanged("COLUMN_NAME");
					this.OnCOLUMN_NAMEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DISPLAY_NAME", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string DISPLAY_NAME
		{
			get
			{
				return this._DISPLAY_NAME;
			}
			set
			{
				if ((this._DISPLAY_NAME != value))
				{
					this.OnDISPLAY_NAMEChanging(value);
					this.SendPropertyChanging();
					this._DISPLAY_NAME = value;
					this.SendPropertyChanged("DISPLAY_NAME");
					this.OnDISPLAY_NAMEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DESCRIPTION", DbType="NVarChar(150)")]
		public string DESCRIPTION
		{
			get
			{
				return this._DESCRIPTION;
			}
			set
			{
				if ((this._DESCRIPTION != value))
				{
					this.OnDESCRIPTIONChanging(value);
					this.SendPropertyChanging();
					this._DESCRIPTION = value;
					this.SendPropertyChanged("DESCRIPTION");
					this.OnDESCRIPTIONChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CATEGORY1", DbType="Int NOT NULL")]
		public int CATEGORY1
		{
			get
			{
				return this._CATEGORY1;
			}
			set
			{
				if ((this._CATEGORY1 != value))
				{
					this.OnCATEGORY1Changing(value);
					this.SendPropertyChanging();
					this._CATEGORY1 = value;
					this.SendPropertyChanged("CATEGORY1");
					this.OnCATEGORY1Changed();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CATEGORY2", DbType="Int NOT NULL")]
		public int CATEGORY2
		{
			get
			{
				return this._CATEGORY2;
			}
			set
			{
				if ((this._CATEGORY2 != value))
				{
					this.OnCATEGORY2Changing(value);
					this.SendPropertyChanging();
					this._CATEGORY2 = value;
					this.SendPropertyChanged("CATEGORY2");
					this.OnCATEGORY2Changed();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_QUERY", DbType="NVarChar(MAX) NOT NULL", CanBeNull=false)]
		public string QUERY
		{
			get
			{
				return this._QUERY;
			}
			set
			{
				if ((this._QUERY != value))
				{
					this.OnQUERYChanging(value);
					this.SendPropertyChanging();
					this._QUERY = value;
					this.SendPropertyChanged("QUERY");
					this.OnQUERYChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SERVER_SELECTION", DbType="Int NOT NULL")]
		public int SERVER_SELECTION
		{
			get
			{
				return this._SERVER_SELECTION;
			}
			set
			{
				if ((this._SERVER_SELECTION != value))
				{
					this.OnSERVER_SELECTIONChanging(value);
					this.SendPropertyChanging();
					this._SERVER_SELECTION = value;
					this.SendPropertyChanged("SERVER_SELECTION");
					this.OnSERVER_SELECTIONChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_INDV_STAT", DbType="Bit NOT NULL")]
		public bool INDV_STAT
		{
			get
			{
				return this._INDV_STAT;
			}
			set
			{
				if ((this._INDV_STAT != value))
				{
					this.OnINDV_STATChanging(value);
					this.SendPropertyChanging();
					this._INDV_STAT = value;
					this.SendPropertyChanged("INDV_STAT");
					this.OnINDV_STATChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MIN_VALUE", DbType="Int")]
		public System.Nullable<int> MIN_VALUE
		{
			get
			{
				return this._MIN_VALUE;
			}
			set
			{
				if ((this._MIN_VALUE != value))
				{
					this.OnMIN_VALUEChanging(value);
					this.SendPropertyChanging();
					this._MIN_VALUE = value;
					this.SendPropertyChanged("MIN_VALUE");
					this.OnMIN_VALUEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MAX_VALUE", DbType="Int")]
		public System.Nullable<int> MAX_VALUE
		{
			get
			{
				return this._MAX_VALUE;
			}
			set
			{
				if ((this._MAX_VALUE != value))
				{
					this.OnMAX_VALUEChanging(value);
					this.SendPropertyChanging();
					this._MAX_VALUE = value;
					this.SendPropertyChanged("MAX_VALUE");
					this.OnMAX_VALUEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_INC_TOLERANCE", DbType="Int")]
		public System.Nullable<int> INC_TOLERANCE
		{
			get
			{
				return this._INC_TOLERANCE;
			}
			set
			{
				if ((this._INC_TOLERANCE != value))
				{
					this.OnINC_TOLERANCEChanging(value);
					this.SendPropertyChanging();
					this._INC_TOLERANCE = value;
					this.SendPropertyChanged("INC_TOLERANCE");
					this.OnINC_TOLERANCEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DEC_TOLERANCE", DbType="Int")]
		public System.Nullable<int> DEC_TOLERANCE
		{
			get
			{
				return this._DEC_TOLERANCE;
			}
			set
			{
				if ((this._DEC_TOLERANCE != value))
				{
					this.OnDEC_TOLERANCEChanging(value);
					this.SendPropertyChanging();
					this._DEC_TOLERANCE = value;
					this.SendPropertyChanged("DEC_TOLERANCE");
					this.OnDEC_TOLERANCEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_INTERVAL", DbType="Int NOT NULL")]
		public int INTERVAL
		{
			get
			{
				return this._INTERVAL;
			}
			set
			{
				if ((this._INTERVAL != value))
				{
					this.OnINTERVALChanging(value);
					this.SendPropertyChanging();
					this._INTERVAL = value;
					this.SendPropertyChanged("INTERVAL");
					this.OnINTERVALChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_STATUS", DbType="Int NOT NULL")]
		public int STATUS
		{
			get
			{
				return this._STATUS;
			}
			set
			{
				if ((this._STATUS != value))
				{
					this.OnSTATUSChanging(value);
					this.SendPropertyChanging();
					this._STATUS = value;
					this.SendPropertyChanged("STATUS");
					this.OnSTATUSChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CREATE_DATE", DbType="DateTime NOT NULL")]
		public System.DateTime CREATE_DATE
		{
			get
			{
				return this._CREATE_DATE;
			}
			set
			{
				if ((this._CREATE_DATE != value))
				{
					this.OnCREATE_DATEChanging(value);
					this.SendPropertyChanging();
					this._CREATE_DATE = value;
					this.SendPropertyChanged("CREATE_DATE");
					this.OnCREATE_DATEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_LAST_RUN_DATE", DbType="DateTime")]
		public System.Nullable<System.DateTime> LAST_RUN_DATE
		{
			get
			{
				return this._LAST_RUN_DATE;
			}
			set
			{
				if ((this._LAST_RUN_DATE != value))
				{
					this.OnLAST_RUN_DATEChanging(value);
					this.SendPropertyChanging();
					this._LAST_RUN_DATE = value;
					this.SendPropertyChanged("LAST_RUN_DATE");
					this.OnLAST_RUN_DATEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SYSTEM_METRIC", DbType="Bit NOT NULL")]
		public bool SYSTEM_METRIC
		{
			get
			{
				return this._SYSTEM_METRIC;
			}
			set
			{
				if ((this._SYSTEM_METRIC != value))
				{
					this.OnSYSTEM_METRICChanging(value);
					this.SendPropertyChanging();
					this._SYSTEM_METRIC = value;
					this.SendPropertyChanged("SYSTEM_METRIC");
					this.OnSYSTEM_METRICChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_EARLY_STAT_DATE", DbType="DateTime")]
		public System.Nullable<System.DateTime> EARLY_STAT_DATE
		{
			get
			{
				return this._EARLY_STAT_DATE;
			}
			set
			{
				if ((this._EARLY_STAT_DATE != value))
				{
					this.OnEARLY_STAT_DATEChanging(value);
					this.SendPropertyChanging();
					this._EARLY_STAT_DATE = value;
					this.SendPropertyChanged("EARLY_STAT_DATE");
					this.OnEARLY_STAT_DATEChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.LNL_DB")]
	public partial class LNL_DB : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _LNL_DBID;
		
		private string _SERVERNAME;
		
		private string _DSN;
		
		private int _LNL_SRV_TYPE;
		
		private System.Nullable<int> _LNL_DBID_MASTER;
		
		private System.Nullable<int> _DB_ENGINE_TYPE;
		
		private string _DB_NAME;
		
		private string _REPL_LOCATION;
		
		private string _MASTER_LOGIN_DRIVER_LOCATION;
		
		private string _VIRTUAL_SERVER_NAME;
		
		private string _NODE_NAME;
		
		private string _ID_ALLOCATION_SERVICE_MACHINE;
		
		private int _PURGE_SUCCESS_TRANS_AFTER_DAYS;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnLNL_DBIDChanging(int value);
    partial void OnLNL_DBIDChanged();
    partial void OnSERVERNAMEChanging(string value);
    partial void OnSERVERNAMEChanged();
    partial void OnDSNChanging(string value);
    partial void OnDSNChanged();
    partial void OnLNL_SRV_TYPEChanging(int value);
    partial void OnLNL_SRV_TYPEChanged();
    partial void OnLNL_DBID_MASTERChanging(System.Nullable<int> value);
    partial void OnLNL_DBID_MASTERChanged();
    partial void OnDB_ENGINE_TYPEChanging(System.Nullable<int> value);
    partial void OnDB_ENGINE_TYPEChanged();
    partial void OnDB_NAMEChanging(string value);
    partial void OnDB_NAMEChanged();
    partial void OnREPL_LOCATIONChanging(string value);
    partial void OnREPL_LOCATIONChanged();
    partial void OnMASTER_LOGIN_DRIVER_LOCATIONChanging(string value);
    partial void OnMASTER_LOGIN_DRIVER_LOCATIONChanged();
    partial void OnVIRTUAL_SERVER_NAMEChanging(string value);
    partial void OnVIRTUAL_SERVER_NAMEChanged();
    partial void OnNODE_NAMEChanging(string value);
    partial void OnNODE_NAMEChanged();
    partial void OnID_ALLOCATION_SERVICE_MACHINEChanging(string value);
    partial void OnID_ALLOCATION_SERVICE_MACHINEChanged();
    partial void OnPURGE_SUCCESS_TRANS_AFTER_DAYSChanging(int value);
    partial void OnPURGE_SUCCESS_TRANS_AFTER_DAYSChanged();
    #endregion
		
		public LNL_DB()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_LNL_DBID", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int LNL_DBID
		{
			get
			{
				return this._LNL_DBID;
			}
			set
			{
				if ((this._LNL_DBID != value))
				{
					this.OnLNL_DBIDChanging(value);
					this.SendPropertyChanging();
					this._LNL_DBID = value;
					this.SendPropertyChanged("LNL_DBID");
					this.OnLNL_DBIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SERVERNAME", DbType="NVarChar(128)")]
		public string SERVERNAME
		{
			get
			{
				return this._SERVERNAME;
			}
			set
			{
				if ((this._SERVERNAME != value))
				{
					this.OnSERVERNAMEChanging(value);
					this.SendPropertyChanging();
					this._SERVERNAME = value;
					this.SendPropertyChanged("SERVERNAME");
					this.OnSERVERNAMEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DSN", DbType="NVarChar(32)")]
		public string DSN
		{
			get
			{
				return this._DSN;
			}
			set
			{
				if ((this._DSN != value))
				{
					this.OnDSNChanging(value);
					this.SendPropertyChanging();
					this._DSN = value;
					this.SendPropertyChanged("DSN");
					this.OnDSNChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_LNL_SRV_TYPE", DbType="Int NOT NULL")]
		public int LNL_SRV_TYPE
		{
			get
			{
				return this._LNL_SRV_TYPE;
			}
			set
			{
				if ((this._LNL_SRV_TYPE != value))
				{
					this.OnLNL_SRV_TYPEChanging(value);
					this.SendPropertyChanging();
					this._LNL_SRV_TYPE = value;
					this.SendPropertyChanged("LNL_SRV_TYPE");
					this.OnLNL_SRV_TYPEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_LNL_DBID_MASTER", DbType="Int")]
		public System.Nullable<int> LNL_DBID_MASTER
		{
			get
			{
				return this._LNL_DBID_MASTER;
			}
			set
			{
				if ((this._LNL_DBID_MASTER != value))
				{
					this.OnLNL_DBID_MASTERChanging(value);
					this.SendPropertyChanging();
					this._LNL_DBID_MASTER = value;
					this.SendPropertyChanged("LNL_DBID_MASTER");
					this.OnLNL_DBID_MASTERChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DB_ENGINE_TYPE", DbType="Int")]
		public System.Nullable<int> DB_ENGINE_TYPE
		{
			get
			{
				return this._DB_ENGINE_TYPE;
			}
			set
			{
				if ((this._DB_ENGINE_TYPE != value))
				{
					this.OnDB_ENGINE_TYPEChanging(value);
					this.SendPropertyChanging();
					this._DB_ENGINE_TYPE = value;
					this.SendPropertyChanged("DB_ENGINE_TYPE");
					this.OnDB_ENGINE_TYPEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DB_NAME", DbType="NVarChar(255)")]
		public string DB_NAME
		{
			get
			{
				return this._DB_NAME;
			}
			set
			{
				if ((this._DB_NAME != value))
				{
					this.OnDB_NAMEChanging(value);
					this.SendPropertyChanging();
					this._DB_NAME = value;
					this.SendPropertyChanged("DB_NAME");
					this.OnDB_NAMEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_REPL_LOCATION", DbType="NVarChar(128)")]
		public string REPL_LOCATION
		{
			get
			{
				return this._REPL_LOCATION;
			}
			set
			{
				if ((this._REPL_LOCATION != value))
				{
					this.OnREPL_LOCATIONChanging(value);
					this.SendPropertyChanging();
					this._REPL_LOCATION = value;
					this.SendPropertyChanged("REPL_LOCATION");
					this.OnREPL_LOCATIONChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MASTER_LOGIN_DRIVER_LOCATION", DbType="NVarChar(128)")]
		public string MASTER_LOGIN_DRIVER_LOCATION
		{
			get
			{
				return this._MASTER_LOGIN_DRIVER_LOCATION;
			}
			set
			{
				if ((this._MASTER_LOGIN_DRIVER_LOCATION != value))
				{
					this.OnMASTER_LOGIN_DRIVER_LOCATIONChanging(value);
					this.SendPropertyChanging();
					this._MASTER_LOGIN_DRIVER_LOCATION = value;
					this.SendPropertyChanged("MASTER_LOGIN_DRIVER_LOCATION");
					this.OnMASTER_LOGIN_DRIVER_LOCATIONChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_VIRTUAL_SERVER_NAME", DbType="NVarChar(255)")]
		public string VIRTUAL_SERVER_NAME
		{
			get
			{
				return this._VIRTUAL_SERVER_NAME;
			}
			set
			{
				if ((this._VIRTUAL_SERVER_NAME != value))
				{
					this.OnVIRTUAL_SERVER_NAMEChanging(value);
					this.SendPropertyChanging();
					this._VIRTUAL_SERVER_NAME = value;
					this.SendPropertyChanged("VIRTUAL_SERVER_NAME");
					this.OnVIRTUAL_SERVER_NAMEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_NODE_NAME", DbType="NVarChar(255)")]
		public string NODE_NAME
		{
			get
			{
				return this._NODE_NAME;
			}
			set
			{
				if ((this._NODE_NAME != value))
				{
					this.OnNODE_NAMEChanging(value);
					this.SendPropertyChanging();
					this._NODE_NAME = value;
					this.SendPropertyChanged("NODE_NAME");
					this.OnNODE_NAMEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID_ALLOCATION_SERVICE_MACHINE", DbType="NVarChar(255)")]
		public string ID_ALLOCATION_SERVICE_MACHINE
		{
			get
			{
				return this._ID_ALLOCATION_SERVICE_MACHINE;
			}
			set
			{
				if ((this._ID_ALLOCATION_SERVICE_MACHINE != value))
				{
					this.OnID_ALLOCATION_SERVICE_MACHINEChanging(value);
					this.SendPropertyChanging();
					this._ID_ALLOCATION_SERVICE_MACHINE = value;
					this.SendPropertyChanged("ID_ALLOCATION_SERVICE_MACHINE");
					this.OnID_ALLOCATION_SERVICE_MACHINEChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PURGE_SUCCESS_TRANS_AFTER_DAYS", DbType="Int NOT NULL")]
		public int PURGE_SUCCESS_TRANS_AFTER_DAYS
		{
			get
			{
				return this._PURGE_SUCCESS_TRANS_AFTER_DAYS;
			}
			set
			{
				if ((this._PURGE_SUCCESS_TRANS_AFTER_DAYS != value))
				{
					this.OnPURGE_SUCCESS_TRANS_AFTER_DAYSChanging(value);
					this.SendPropertyChanging();
					this._PURGE_SUCCESS_TRANS_AFTER_DAYS = value;
					this.SendPropertyChanged("PURGE_SUCCESS_TRANS_AFTER_DAYS");
					this.OnPURGE_SUCCESS_TRANS_AFTER_DAYSChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
