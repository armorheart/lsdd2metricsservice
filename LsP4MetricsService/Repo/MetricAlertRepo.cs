﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LsP4MetricsService.Model;
using MetricsDash.Math;
using MetricsDash.Models;

namespace LsP4MetricsService.Repo
{
    public class MetricAlertRepo : IMetricAlertRepo
    {
        public MetricAlertRepo()
        {
            
        }

        public IList<Alert> GetMetricAlerts(RealtimeStatConfig metric)
        {
            var alerts = new List<Alert>();

            var max = metric.MaxValue ?? 0;
            var min = metric.MinValue ?? 0;

            //Check Max Value
            foreach (var region in metric.Values)
            {
                var val = region.Value ?? 0;
                if (val > max)
                {
                    alerts.Add(AddAlert(metric, val, max, region));
                }
                if (val < min)
                {
                    alerts.Add(AddAlert(metric, val, min, region));
                }
            }

            return alerts;
        }

        private Alert AddAlert(RealtimeStatConfig metric, int val, int max, RealtimeValue region)
        {
            return new Alert()
            {
                Level = AlertLevel.Critical,
                MetricName = metric.DisplayName,
                Percent = Formulas.Percent(val , max),
                Region = region.Server,
                TimeStamp = region.TimeStamp ?? DateTime.UtcNow,
                Value = val
            };
        }
    }
}
