using System;
using System.Collections.Generic;
using LsP4MetricsService.Model;
using MetricsDash.Models;
using Phase4Lib.Web.Models;

namespace LsP4MetricsService.Repo
{
    public interface IMetricsServiceRepo
    {
        string MasterConStr { get; set; }
        Dictionary<string, ConnectorConfig> RegionConnStrs { get; }
        List<ErrorVm> Errors { get; }

        /// <summary>
        /// Gets list of statistics to gather
        /// Each stat has it's own query
        /// </summary>
        /// <returns>A List of enabled metrics</returns>
        IEnumerable<StatConfig> GetMetricList();

        IEnumerable<RealtimeStatConfig> GetRealtimeList(); 

        void InitRegions();
        int InitHistory();
        void InitEarlyDate();

        /// <summary>
        /// Inserts lines into the History table for the current series
        /// </summary>
        /// <param name="seriesTimestamp"></param>
        /// <param name="seriesId"></param>
        void InsertHistorySeries(DateTime seriesTimestamp, Guid seriesId, Interval interval);


        /// <summary>
        /// Runs a metric and updates history table
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="seriesTimestamp"></param>
        /// <param name="id"></param>
        /// <param name="seriesId"></param>
        /// <returns></returns>
        bool RunMetric(StatConfig stat, DateTime seriesTimestamp, Guid seriesId);

        bool RunRealTime(RealtimeStatConfig stat);

        DateTime GetBeginInterval(DateTime timestamp, Interval interval);
        DateTime GetEndInterval(DateTime timestamp, Interval interval);
    }
}