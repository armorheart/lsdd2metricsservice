﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel;
using Castle.Windsor;
using MetricsDash.Cached;

namespace LsP4MetricsService.Factories
{
    /// <summary>
    /// Update this to use Castle Windsor Factory Facility
    /// It will get rid of needing to resolve objects directly
    /// </summary>
    public class BasicCachedDataItemsFactory
    {
        private readonly IKernel _container;

        public BasicCachedDataItemsFactory(IKernel container)
        {
            _container = container;
        }

        public List<ICachedItemQuery> GetAll()
        {

            return _container.ResolveAll<ICachedItemQuery>().ToList();
        }
    }
}
