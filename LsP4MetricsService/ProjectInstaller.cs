﻿using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;

namespace LsP4MetricsService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        private void serviceProcessInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
            //Create source for Event Log
            if (!EventLog.SourceExists("P4MetricsService"))
            {
                EventLog.CreateEventSource("P4MetricsService", "Application");
            }
        }
    }
}
