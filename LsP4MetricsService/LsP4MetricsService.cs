﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Threading;
using Castle.Windsor;
using LsP4MetricsService.Config;
using LsP4MetricsService.Jobs;
using LsP4MetricsService.Repo;
using MetricsDash.Models;
using Phase4Lib.Injection;
using Phase4Lib.Service;
using Quartz;
using Quartz.Impl;


namespace LsP4MetricsService
{
    public partial class LsP4MetricsService : P4ServiceBase
    {
        private readonly IWindsorContainer _Container;

        private Thread _AlertThread;

        private bool _AlertThreadRunning;

        private IMetricsServiceRepo _MetricsRepo;
        private EmailConfig _EmailConfig;

        private static readonly log4net.ILog _Log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Quartz Scheduling
        private ISchedulerFactory _Factory;
        private IScheduler _Scheduler;
        
        public LsP4MetricsService(IWindsorContainer container)
        {
            _Container = container;
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                
                //Set connection strings
                var config = MetricsConfig.GetConfig();

                var master = ConfigurationManager.ConnectionStrings["DD2Database"];

                if (master != null)
                {
                    _MetricsRepo = _Container.Resolve<IMetricsServiceRepo>();
                    _MetricsRepo.InitRegions();
                   
                    _EmailConfig = config.Email;
                }
                else
                {
                    throw new Exception("Master ConnectionString not set");
                }

            }
            catch (Exception ex)
            {
                _Log.Fatal("OnStart", ex);
                throw;
            }

            //Make the columns right
            _MetricsRepo.InitHistory();

            //Set the earliest date for each stat if nessesary
            _MetricsRepo.InitEarlyDate();

            
#if DEBUG
            //var cachedJob = _Container.Resolve<CachedQueryJob>();
            //cachedJob.RunJob(MetricsConfig.GetConfig(), ConfigurationManager.ConnectionStrings["DD2Database"].ConnectionString);

            ////var metricsJob = new MetricsJob(_MetricsRepo);
            ////metricsJob.RunMetrics(MetricsConfig.GetConfig(), ConfigurationManager.ConnectionStrings["DD2Database"].ConnectionString, (int)MetricsDash.Models.Interval.Monthly);
            //var realtimeJob = new RealtimeMetricJob(_MetricsRepo);
            //realtimeJob.RunJob(MetricsConfig.GetConfig(), ConfigurationManager.ConnectionStrings["DD2Database"].ConnectionString);
#else

            
#endif

            SetupSchedule();

            var temp = false;



            //_AlertThreadRunning = true;
            //_AlertThread = new Thread(RunAlert);
            //_AlertThread.Start();
        }

       

        protected override void OnStop()
        {
            _AlertThreadRunning = false;
        }

        private void RunMissedIntervals()
        {
            
        }

        private void SetupSchedule()
        {
            //Set Quartz to use Sql Server to Serialize jobs and retry one if we miss the scheduled time
            //var props = new NameValueCollection();
            //props["quartz.jobStore.type"] = "Quartz.Impl.AdoJobStore.JobStoreTX, Quartz";
            //props["quartz.jobStore.tablePrefix"] = "UDO_DD2_QRTZ_";
            //props["quartz.jobStore.driverDelegateType"] = "Quartz.Impl.AdoJobStore.SqlServerDelegate, Quartz";
            //props["quartz.jobStore.dataSource"] = "default";
            //props["quartz.dataSource.default.connectionString"] =
            //    ConfigurationManager.ConnectionStrings["DD2Database"].ConnectionString;
            //props["quartz.dataSource.default.provider"] = "SqlServer-20";
            //props["quartz.jobStore.useProperties"] = "true";


            _Factory = new StdSchedulerFactory();

            
            _Scheduler = _Factory.GetScheduler();
            _Scheduler.JobFactory = new WindsorJobFactory(_Container);
            _Scheduler.Start();

            var config = MetricsConfig.GetConfig();
            var master = ConfigurationManager.ConnectionStrings["DD2Database"].ConnectionString;

            var jobMap = new JobDataMap();
            jobMap.Add("config", config);
            jobMap.Add("master", master);

            IJobDetail metricsJob = JobBuilder.Create<MetricsJob>()
                .WithIdentity("MetricsJob")
                .UsingJobData(jobMap)
                .Build();

            IJobDetail realtimeJob = JobBuilder.Create<RealtimeMetricJob>()
                .WithIdentity("RealtimeJob")
                .UsingJobData(jobMap)
                .Build();

            IJobDetail cachedQueryJob = JobBuilder.Create<CachedQueryJob>()
                .WithIdentity("CachedQueryJob")
                .UsingJobData(jobMap)
                .Build();

            ITrigger monthlyTrigger = TriggerBuilder.Create()
                .WithIdentity("monthly")
                .UsingJobData("interval", (int)Interval.Monthly)
                .StartNow()
                .WithSchedule(CronScheduleBuilder.MonthlyOnDayAndHourAndMinute(1, 2, 0))
                .Build();

            ITrigger hourlyTrigger = TriggerBuilder.Create()
                .WithIdentity("hourly")
                .UsingJobData("interval", (int)Interval.Hourly)
                .StartNow()
                .WithSimpleSchedule(s => s.WithIntervalInHours(1).RepeatForever())
                .Build();

            ITrigger min10Trigger = TriggerBuilder.Create()
                .WithIdentity("min10")
                .StartNow()
                .WithSimpleSchedule(s => s.WithIntervalInMinutes(10).RepeatForever())
                .Build();

            var metricsTrigger = _Scheduler.ScheduleJob(metricsJob, monthlyTrigger).ToLocalTime().ToString();
            var realtimeTrigger = _Scheduler.ScheduleJob(realtimeJob, hourlyTrigger).ToLocalTime().ToString();
            var cachedtime = _Scheduler.ScheduleJob(cachedQueryJob, min10Trigger).ToLocalTime().ToString();

            var temp = 0;





        }

        
    }


}
