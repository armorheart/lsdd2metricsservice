﻿using System.Configuration;

namespace LsP4MetricsService.Config
{
    internal class MetricsConfig : ConfigurationSection
    {
        public static MetricsConfig GetConfig()
        {
            return ConfigurationManager.GetSection("MetricsConfig") as MetricsConfig;
        }

        [ConfigurationProperty("Email", IsRequired = false)]
        public EmailConfig Email
        {
            get { return (EmailConfig) this["Email"]; }
        }



    }

    

    internal class EmailConfig : ConfigurationElement
    {
        [ConfigurationProperty("From", IsRequired = true)]
        public string From
        {
            get { return (string) this["From"]; }
        }

        [ConfigurationProperty("Server", IsRequired = true)]
        public string Server
        {
            get { return (string) this["Server"]; }
        }

        [ConfigurationProperty("AdminTo", IsRequired = true)]
        public string AdminTo
        {
            get { return (string) this["AdminTo"]; }
        }
    }

}
            