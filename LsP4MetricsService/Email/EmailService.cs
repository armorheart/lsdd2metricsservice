﻿using Phase4Lib.Web.Mvc.Interfaces;
using Phase4Lib.Web.Mvc;

namespace LsP4MetricsService.Email
{
    public class Emailsettings
    {
        public string Server { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
    }
    public class EmailService
    {
        private readonly ITemplatesService _TemplatesService;
        public EmailService()
        {
            _TemplatesService = new TemplatesService(new FileSystemService(), new RazorTemplateEngine());
        }

        public void SendSummaryEmail(Emailsettings settings, dynamic model)
        {
            SendEmail("summary", settings, model);
        }

        

        public void SendAdminSummary(Emailsettings settings, dynamic model)
        {
            SendEmail("admin", settings, model);
        }

        public void SendNotificationEmail(Emailsettings settings, dynamic model)
        {
            SendEmail("alert", settings, model );
        }

        private void SendEmail(string templateName, Emailsettings settings, dynamic model)
        {
            var body = _TemplatesService.Parse(templateName, model);
            var mail = new Phase4Lib.Email.SmtpMail(settings.Server, 25);

            mail.SendEmail(settings.From, settings.To, settings.Subject, body, null);
        }

        
    }
}
