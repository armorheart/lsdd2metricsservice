﻿using System.Globalization;
using System.IO;
using Phase4Lib.Web.Mvc.Interfaces;

namespace LsP4MetricsService.Email
{
    class TemplatesService : ITemplatesService
    {
        private const string DefaultLanguage = "en";
        private const string TemplatesDirectoryName = "Templates";
        private const string TemplateFileNameWithCultureTemplate = "{0}.{1}.template";
        private const string TemplateFileNameWithoutCultureTemplate = "{0}.template";

        private readonly IFileSystemService _FileSystemService;
        private readonly ITemplateEngine _TemplateEngine;
        private readonly string _TemplatesDirFullName;

        public TemplatesService(IFileSystemService fileSystemService, ITemplateEngine templateEngine)
        {
            _FileSystemService = fileSystemService;
            _TemplateEngine = templateEngine;
            _TemplatesDirFullName = Path.Combine(_FileSystemService.GetCurrentDirectory(), TemplatesDirectoryName);
        }

        public string Parse(string templateName, dynamic model, CultureInfo cultureInfo = null)
        {
            var templateContent = GetContent(templateName, cultureInfo);
            return _TemplateEngine.Parse(templateContent, model);
        }

        private string GetContent(string templateName, CultureInfo cultureInfo)
        {
            var templateFileName = TryGetFileName(templateName, cultureInfo);
            if (string.IsNullOrWhiteSpace(templateFileName))
            {
                throw new FileNotFoundException(string.Format("Template file not found for template '{0}' in '{1}'",
                                                              templateName, _TemplatesDirFullName));
            }

            return _FileSystemService.ReadAllText(templateFileName);
        }

        private string TryGetFileName(string templateName, CultureInfo cultureInfo)
        {
            var language = GetLanguageName(cultureInfo);

            // check file for current culture
            var fullFileName = GetFullFileName(templateName, language);
            if (_FileSystemService.FileExists(fullFileName))
            {
                return fullFileName;
            }

            // check file for default culture
            if (language != DefaultLanguage)
            {
                fullFileName = GetFullFileName(templateName, DefaultLanguage);
                if (_FileSystemService.FileExists(fullFileName))
                {
                    return fullFileName;
                }
            }

            // check file without culture
            fullFileName = GetFullFileName(templateName, string.Empty);
            if (_FileSystemService.FileExists(fullFileName))
            {
                return fullFileName;
            }

            return string.Empty;
        }

        private string GetFullFileName(string templateName, string language)
        {
            var fileNameTemplate = string.IsNullOrEmpty(language) ? TemplateFileNameWithoutCultureTemplate : TemplateFileNameWithCultureTemplate;
            var templateFileName = string.Format(fileNameTemplate, templateName, language);
            return Path.Combine(_TemplatesDirFullName, templateFileName);
        }

        private string GetLanguageName(CultureInfo cultureInfo)
        {
            return cultureInfo != null ? cultureInfo.TwoLetterISOLanguageName.ToLower() : DefaultLanguage;
        }
    }
}