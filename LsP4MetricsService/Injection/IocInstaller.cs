﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using LsP4MetricsService.Factories;
using MetricsDash.Cached;
using MetricsDash.Models;
using MetricsDash.Repo;
using MetricsDash.Repo.Interfaces;
using Phase4Lib.Audit;
using Quartz;

namespace LsP4MetricsService.Injection
{
    public class IocInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var connName = "DD2Database";
            var connStr = ConfigurationManager.ConnectionStrings[connName].ConnectionString;

            container.Register(
                Classes.FromAssemblyNamed("MetricsDash")
                    .BasedOn<ICachedItemQuery>()
                    .WithServiceSelf()
                    .WithServiceFirstInterface()
                    .Configure(c => c.Named(c.Implementation.Name))
                );

            container.Register(
                Component.For<IMetricsRepo<StatConfig>>()
                .ImplementedBy<MetricsRepo>()
               .Named("MetricsRepo")
                .LifestyleTransient()
                .DependsOn(Dependency.OnValue("connStr", connStr),
                        Dependency.OnValue("connectionName", connName)),
                  Component.For<IMetricsRepo<RealtimeStatConfig>>()
                .ImplementedBy<RealtimeMetricsRepo>()
                .Named("RealtimeMetricsRepo")
                .LifestyleTransient()
                .DependsOn(Dependency.OnValue("connStr", connStr),
                        Dependency.OnValue("connectionName", connName)),
                Classes.FromAssemblyNamed("MetricsDash")
                    .Pick()
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient()
                    .Configure(i => i.DependsOn(
                        Dependency.OnValue("connStr", connStr),
                        Dependency.OnValue("connectionName", connName))
                    )
                );
            container.Register(
                Classes.FromAssemblyNamed("Phase4Lib")
                    .Pick()
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient()
                    .Configure(i => i.DependsOn(
                        Dependency.OnValue("connectionName", connName))
                    )
                );

            container.Register(
                Classes.FromThisAssembly()
                    .BasedOn<IJob>()
                    .WithServiceSelf()
                    .WithServiceFirstInterface()
                    .Configure(c => c.Named(c.Implementation.Name)));

            container.Register(
                Component.For<BasicCachedDataItemsFactory>()
                .ImplementedBy<BasicCachedDataItemsFactory>(),
                Classes.FromThisAssembly()
                    .Pick()
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient()
                    .Configure(i => i.DependsOn(
                        Dependency.OnValue("connStr", connStr),
                        Dependency.OnValue("connectionName", connName))
                    )
                );

            
        }
    }
}
