﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LsP4MetricsService.Model
{
    public enum AlertLevel
    {
        None,
        Warning,
        Critical
    }
}
