﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LsP4MetricsService.Model
{
    public class AdminSummaryVm
    {
        public List<ErrorVm> Errors { get; set; }
        public int Total { get; set; }
        public DateTime StartInterval { get; set; }
        public DateTime EndInterval { get; set; }
        
        public string Subject { get
        {
            return string.Format("Metrics Summary for {0} - {1}: {2} Total {3}", StartInterval, EndInterval, Total,
                Errors.Any() ? string.Format(", {0} Errors", Errors.Count) : "");
        } }

        public AdminSummaryVm()
        {
            Errors = new List<ErrorVm>();
        }
    }
}
