﻿using MetricsDash.Models;

namespace LsP4MetricsService.Model
{
    public class MetricVM
    {
        public int Value { get; protected set; }
        public string Category { get; protected set; }
        public string Change { get; protected set; }
        public string Health { get; protected set; }
        public string Tolerance { get; protected set; }

        public StatConfig Metric { get; set; }

        public MetricVM()
        {
            Metric = new StatConfig();
        }

        public MetricVM(int value, StatConfig metric)
        {
            Value = value;
            Metric = metric;

            Category = metric.Category1Ref.Name;
            
            //replace this with logic
            Health = "Normal";
            int dec = metric.DecTolerance ?? 0;
            int inc = metric.IncTolerance ?? 0;

            Tolerance = string.Format("-{0} / +{1}", inc.ToString("p"), dec.ToString("p"));

        }
    }
}
