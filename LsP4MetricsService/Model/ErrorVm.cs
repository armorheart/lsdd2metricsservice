﻿using System;

namespace LsP4MetricsService.Model
{
    public class ErrorVm
    {
        public DateTime TimeStamp { get; set; }
        public string Stat { get; set; }
        public string Error { get; set; }
        public Exception Exception { get; set; }
        public string Server { get; set; }
    }
}
