﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LsP4MetricsService.Model
{
    public class Alert
    {
        public string MetricName { get; set; }
        public string Region { get; set; }
        public int Value { get; set; }
        public double Percent { get; set; }
        public DateTime TimeStamp { get; set; }

        public AlertLevel Level { get; set; }

        public string GetDateStr()
        {
            return TimeStamp.ToLongDateString();
        }
    }
}
