﻿using System;
using MetricsDash.Models;

namespace LsP4MetricsService.Model
{

    public static class IntervalEval
    {
        /// <summary>
        /// Minutes in a Month (30 days)
        /// </summary>
        public static int Month { get { return 43200; } }


        /// <summary>
        /// Gets next time the interval should be run
        /// </summary>
        /// <param name="lastRun"></param>
        /// <param name="interval"></param>
        /// <returns></returns>
        public static DateTime GetNext(DateTime? lastRun, int? interval)
        {
            //The shortest interval is currently Daily, so get the date part of lastrun
            //var lastday = lastRun.Date;
            if (lastRun == null)
            {
                return DateTime.UtcNow;
            }

           //1 Month is the default
            return ((DateTime)lastRun).AddMinutes(interval ?? Month);

            
        }

        public static DateTime GetNext(DateTime lastday, Interval interval)
        {
            switch (interval)
            {
                case Interval.Daily:
                    return lastday.AddDays(1);
                case Interval.Weekly:
                    return lastday.AddDays(7);
                case Interval.Monthly:
                    return lastday.AddMonths(1);
                default:
                    throw new ArgumentOutOfRangeException("interval");
            }
        }
    }
    
}
