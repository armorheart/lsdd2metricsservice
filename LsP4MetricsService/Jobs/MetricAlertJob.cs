﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LsP4MetricsService.Config;
using LsP4MetricsService.Email;
using LsP4MetricsService.Model;
using LsP4MetricsService.Repo;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;
using MetricsDash.UserManagement;
using Phase4Lib.Web.Models;
using Quartz;

namespace LsP4MetricsService.Jobs
{
    public class MetricAlertJob : IJob
    {
        private readonly IMetricsRepo<RealtimeStatConfig> _RealtimeRepo;
        private readonly IUserRepo _UserRepo;
        private readonly IMetricAlertRepo _AlertRepo;

        private static readonly log4net.ILog _Log = log4net.LogManager.GetLogger
          (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MetricAlertJob(IMetricsRepo<RealtimeStatConfig> realtimeRepo,
            IUserRepo userRepo,
            IMetricAlertRepo alertRepo)
        {
            _RealtimeRepo = realtimeRepo;
            _UserRepo = userRepo;
            _AlertRepo = alertRepo;
        }

        public void Execute(IJobExecutionContext context)
        {
            //get Connection String
            var dataMap = context.MergedJobDataMap;

            var config = dataMap["config"] as MetricsConfig;

            if (config == null)
            {
                throw new JobExecutionException("config is null");
            }


            RunJob(config);
        }

        internal void RunJob(MetricsConfig config)
        {
            try
            {
                //Get Stats
                foreach (var stat in _RealtimeRepo.GetMetrics())
                {
                    //See if stat is in an alerted state
                    var regionsAlerted = _AlertRepo.GetMetricAlerts(stat);

                    if (regionsAlerted.Any())
                    {
                        //Get users who subscribe to this metric's alerts
                        var users = _UserRepo.GetAlertSubscribers(stat);
                        foreach (var user in users)
                        {
                            //Send an alert for each region it's in an alerted state
                            foreach (var alert in regionsAlerted)
                            {
                                SendAlert(config, alert, user);
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                _Log.Error("Unhandled exception in Alert job", e);
            }
        }

        private void SendAlert(MetricsConfig config, Alert alert, MetricUser user)
        {
            var emailService = new EmailService();
            emailService.SendNotificationEmail(new Emailsettings()
            {
                From = config.Email.From,
                Server = config.Email.Server,
                Subject = string.Format("Metric Notification for {0}", alert.MetricName),
                To = user.Email
            }, alert);
            
        }
    }
}
