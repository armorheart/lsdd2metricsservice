﻿using System;
using System.Linq;
using LsP4MetricsService.Config;
using LsP4MetricsService.Email;
using LsP4MetricsService.Model;
using LsP4MetricsService.Repo;
using MetricsDash.Models;
using Quartz;

namespace LsP4MetricsService.Jobs
{
    public class MetricsJob : IJob
    {
        private readonly IMetricsServiceRepo _Repo;

        private static readonly log4net.ILog _Log = log4net.LogManager.GetLogger
           (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MetricsJob(IMetricsServiceRepo repo)
        {
            _Repo = repo;
        }

        public void Execute(IJobExecutionContext context)
        {

            //get Connection String
            var dataMap = context.MergedJobDataMap;

            var config = dataMap["config"] as MetricsConfig;

            if (config == null)
            {
                throw new JobExecutionException("config is null");
            }

            var interval = (int)dataMap["interval"];

            RunMetrics(config, interval);
        }

        internal void RunMetrics(MetricsConfig config, int interval)
        {
            
            //Instanciate Repo
            _Repo.InitRegions();

            //Make sure all the columns exist in the History Table
            _Repo.InitHistory();

            //Set the earliest date for each stat if nessesary
            _Repo.InitEarlyDate();

            //Get Series TimeStamp
            var seriesTimestamp = DateTime.UtcNow;
            var seriesId = Guid.NewGuid();
            var bFirst = true;

            var adminVm = new AdminSummaryVm();
            

            //Get metrics with specified interval
            var metrics = _Repo.GetMetricList();
            foreach (var metric in metrics.Where(m => m.Interval == interval))
            {
                try
                {
                    adminVm.Total++;

                    if (bFirst)
                    {
                        _Repo.InsertHistorySeries(seriesTimestamp, seriesId, (Interval)interval);
                        adminVm.StartInterval = _Repo.GetBeginInterval(seriesTimestamp, (Interval) interval);
                        adminVm.EndInterval = _Repo.GetEndInterval(seriesTimestamp, (Interval) interval);

                        bFirst = false;
                    }

                    if (metric.ColumnName.StartsWith("CONFIG") || metric.ColumnName.StartsWith("USER"))
                    {
                        var msg = "stop";
                    }

                   _Repo.RunMetric(metric, seriesTimestamp, seriesId);

                }
                catch (Exception ex)
                {
                    _Log.Error(string.Format("Unhandled Error running {0}", metric.ColumnName), ex);
                    adminVm.Errors.Add(new ErrorVm()
                    {
                        TimeStamp = DateTime.Now,
                        Stat = metric.ColumnName,
                        Error = ex.Message,
                        Exception = ex
                    });
                }
            }

            adminVm.Errors.AddRange(_Repo.Errors);

            //Send error summary email
            var emailService = new EmailService();
            emailService.SendAdminSummary(new Emailsettings
                    {
                        From = config.Email.From,
                        Server = config.Email.Server,
                        Subject = adminVm.Subject,
                        To = config.Email.AdminTo
                        }, adminVm);

        }
    }
}
