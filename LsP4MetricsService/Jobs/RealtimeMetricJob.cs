﻿using System;
using LsP4MetricsService.Config;
using LsP4MetricsService.Repo;
using Quartz;

namespace LsP4MetricsService.Jobs
{
    public class RealtimeMetricJob : IJob
    {
        private readonly IMetricsServiceRepo _Repo;

        private static readonly log4net.ILog _Log = log4net.LogManager.GetLogger
           (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public RealtimeMetricJob(IMetricsServiceRepo repo)
        {
            _Repo = repo;
        }

        public void Execute(IJobExecutionContext context)
        {
            //get Connection String
            var dataMap = context.MergedJobDataMap;

            var config = dataMap["config"] as MetricsConfig;

            if (config == null)
            {
                throw new JobExecutionException("config is null");
            }

          
            RunJob(config);
        }

        internal void RunJob(MetricsConfig config)
        {
            _Repo.InitRegions();

            //Get metrics with specified interval
            var metrics = _Repo.GetRealtimeList();
            foreach (var metric in metrics)
            {
                try
                {
                    _Repo.RunRealTime(metric);
                }
                catch (Exception ex)
                {
                    _Log.Error(string.Format("Unhandled Error running {0}", metric.ColumnName), ex);
                }
            }
        }
    }
}
