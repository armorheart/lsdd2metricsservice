﻿using System;
using System.Linq;
using LsP4MetricsService.Config;
using MetricsDash.Cached;
using Phase4Lib.Web.DataAccess;
using Phase4Lib.Web.Models;
using Quartz;
using BasicCachedDataItemsFactory = LsP4MetricsService.Factories.BasicCachedDataItemsFactory;

namespace LsP4MetricsService.Jobs
{
    public class CachedQueryJob: IJob
    {
        private readonly BasicCachedDataItemsFactory _factory;

        private static readonly log4net.ILog _Log = log4net.LogManager.GetLogger
           (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CachedQueryJob(BasicCachedDataItemsFactory factory)
        {
            _factory = factory;
        }

        public void Execute(IJobExecutionContext context)
        {
            //get Connection String
            var dataMap = context.MergedJobDataMap;

            var config = dataMap["config"] as MetricsConfig;
            var master = dataMap["master"] as string;

            if (config == null)
            {
                throw new JobExecutionException("config is null");
            }


            RunJob();
        }

        internal void RunJob()
        {
            try
            {
                var commonRepo = new CommonRepo("DD2Database");
                var regions = commonRepo.GetRegions().ToDictionary(k => k.Id);

                //Get cached data items
                foreach (var item in _factory.GetAll())
                {
#if DEBUG
                    if (!(item is WorkforceBadgeCq))
                    {
                        continue;
                        
                    }
#endif

                    foreach (var connector in item.Connectors)
                    {
                        try
                        {
#if DEBUG
                            if (connector == 4)
                            {
                                //Cylinder 3 isn't working from my machine
                                continue;
                            }
#endif

                            item.RunQuery(regions[connector].GetConnectionString(), connector.ToString());
                            //Get the existing cached item
                            var cached = commonRepo.GetCachedData("Metrics", item.Type, connector);
                            if (cached == null)
                            {
                                //add new one
                                cached = new CachedData
                                {
                                    App = "Metrics",
                                    Type = item.Type,
                                    ConnectorId = connector,
                                    TimeStamp = DateTime.UtcNow
                                };

                                commonRepo.AddCachedData(cached, item.GetData());
                            }
                            else
                            {
                                cached.TimeStamp = DateTime.UtcNow;
                                commonRepo.UpdateCachedData(cached, item.GetData());
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            _Log.Error(string.Format("{0} {1}", item.Type, regions[connector].Name), e);
                        }
                    }
                
                }
                _Log.Debug("Done");
            }
            catch (Exception e)
            {
                _Log.Error("Unhandled Cached Query job error", e);
            }
        }
    }
}
