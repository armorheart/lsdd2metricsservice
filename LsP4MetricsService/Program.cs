﻿using Castle.Windsor;
using Castle.Windsor.Installer;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace LsP4MetricsService
{
    static class Program
    {
        static IWindsorContainer CreateContainer()
        {
            var container = new WindsorContainer();
            container.Install(FromAssembly.This());
            return container;
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            
            var service = new LsP4MetricsService(CreateContainer());

            #if DEBUG
                Phase4Lib.Service.ServiceStarter.StartConsole(service);
            #else
                    Phase4Lib.Service.ServiceStarter.StartService(service);
            #endif

        }
    }
}
